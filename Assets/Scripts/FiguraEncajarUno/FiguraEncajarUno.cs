﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FiguraEncajarUno : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;
	
	public TrashFiguraEncajarUno trashCuadrado;
	public TrashFiguraEncajarUno trashCirculo;
	public TrashFiguraEncajarUno trashTriangulo;
	public TrashFiguraEncajarUno trashRectangulo;

	public ItemsFiguraEncajarUno cuadrado;
	public ItemsFiguraEncajarUno circulo;
	public ItemsFiguraEncajarUno triangulo;
	public ItemsFiguraEncajarUno rectangulo;
	//public GameObject[] Items = new GameObject[3];
	public GameObject[] figurasGeometricas = new GameObject[4];

	public int siguienteEscena;
	void Start () {
		escogerForma();
	}
	// Update is called once per frame
	public void escogerForma(){
		
		switch(Random.Range(0,4)){
			case 0:
				figurasGeometricas[0].SetActive(true);
				idFigura=0;
				trashCuadrado.setElemento(0);
				break;
			case 1:
				figurasGeometricas[1].SetActive(true);
				idFigura=1;
				trashCirculo.setElemento(1);
				break;
			case 2:
				figurasGeometricas[2].SetActive(true);
				idFigura=2;
				trashTriangulo.setElemento(2);
				break;
			case 3:
				figurasGeometricas[3].SetActive(true);
				idFigura=3;
				trashRectangulo.setElemento(3);
				break;
		}
	}

	public void comprobar(int idItem){
		Debug.Log("Id Item: "+idItem+" idFigura: "+idFigura);
		switch(idItem){
			case 0:
				if(trashCuadrado.getContador()==1){
					StartCoroutine("cargarSiguiente");
					cuadrado.setPositionTrash(trashCuadrado.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					cuadrado.resetDia();
				}
				break;
			case 1:
				if(trashCirculo.getContador()==1){
					StartCoroutine("cargarSiguiente");
					circulo.setPositionTrash(trashCirculo.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					circulo.resetDia();
				}
				break;
			case 2:
				if(trashTriangulo.getContador()==1){
					StartCoroutine("cargarSiguiente");
					triangulo.setPositionTrash(trashTriangulo.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					triangulo.resetDia();
				}
				break;
			case 3:
				if(trashRectangulo.getContador()==1){
					StartCoroutine("cargarSiguiente");
					rectangulo.setPositionTrash(trashRectangulo.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					rectangulo.resetDia();
				}
				break;
		}
		
	}
	IEnumerator cargarSiguiente(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
			SceneManager.LoadScene(siguienteEscena);
		} else {
			SceneManager.LoadScene("Completada");
		}
	}
	IEnumerator cargarIncorrecto(){
		ValidacionController.cargarResultadoImage(false);
		yield return new WaitForSeconds(2);
	}

}
