﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ComparacionManger3Atributos : MonoBehaviour {

	// Use this for initialization 
	private int izquierada;
	private int derecha;
	private int colorOpcion;
	private int alturaOpcion;
	private int anchoOpcion;
	private int colorOpcionIncorrecto;
	private int alturaOpcionIncorrecto;
	private int anchoOpcionIncorrecto;
	public GameObject[] colorNegro = new GameObject[20];
	public GameObject[] colorVerde = new GameObject[20];
	public GameObject[] colorCafe = new GameObject[20];
	public GameObject[] colorAzul = new GameObject[20];
	public GameObject[] colorRojo = new GameObject[20];
	private int elementoActivo;

	//Rango

	private int rangoMinimo;
	private int rangoMaximo;
	public Text color;
	public Text altura;
	public Text ancho;
	public int correcto = 0;

	void Start () {
			
			
			//ActivarElementos(izquierada);
			//ActivarElementos(derecha);
			iniciarVuelta();
	}
	void Update(){

	}
	private void iniciarVuelta(){
		generarRandomCorrectos();
			correcto = Random.Range(0,2);
			if (correcto == 1){
				Debug.Log("generar correcto");
				deteterinarCorrecto();
			} else {
				Debug.Log("generar incorrecto");
				//deteterinarIncorrecto();
				generarRandomIncorrectos();
			}
			//generarRandomIncorrectos();
			rangoMinimo =0;
			rangoMaximo = 100;
			generarEnunciado();
	}
	private void generarRandomCorrectos() {
		colorOpcion = Random.Range(0, 5);
		alturaOpcion = Random.Range(0,2);
		anchoOpcion = Random.Range(0,2);
	}
	public void generarRandomIncorrectos(){
			colorOpcionIncorrecto = Random.Range(0, 5);
			alturaOpcionIncorrecto = Random.Range(0,2);
			anchoOpcionIncorrecto = Random.Range(0,2);
			if(colorOpcionIncorrecto != colorOpcion || alturaOpcionIncorrecto != alturaOpcion || anchoOpcionIncorrecto != anchoOpcion){
				deteterinarIncorrecto();
			} else {
				generarRandomIncorrectos ();
			}
	}
		public void ActivarElementos(int indice){
		//elementos[indice].SetActive(true);
	}

	public void deteterinarIncorrecto(){
		switch(colorOpcionIncorrecto){
			case 0:						//solo llamar color negro
					rangoMinimo=0;
					rangoMaximo=19;
					Debug.Log("color es 1");
					determinarAnchoIncorrecto (this.rangoMinimo, this.rangoMaximo);
					activarPorColorIncorrecto();
					break;
			case 1:
					rangoMinimo=20;
					rangoMaximo=39;
					Debug.Log("color es 2");
					determinarAnchoIncorrecto (this.rangoMinimo, this.rangoMaximo);
					activarPorColorIncorrecto();
					break;
			case 2:
					rangoMinimo=40;
					rangoMaximo=59;
					Debug.Log("color es 3");
					determinarAnchoIncorrecto (this.rangoMinimo, this.rangoMaximo);
					activarPorColorIncorrecto();
					break;
			case 3:
					rangoMinimo=60;
					rangoMaximo=79;
					Debug.Log("color es 4");
					determinarAnchoIncorrecto (this.rangoMinimo, this.rangoMaximo);
					activarPorColorIncorrecto();
					break;
			case 4:
					rangoMinimo=80;
					rangoMaximo=99;
					Debug.Log("color es 5");
					determinarAnchoIncorrecto (this.rangoMinimo, this.rangoMaximo);
					activarPorColorIncorrecto();
					break;
		}
	}
	public void determinarAnchoIncorrecto(int rangoMinimo, int rangoMaximo){
			if(anchoOpcionIncorrecto==0){
					this.rangoMaximo=this.rangoMaximo-10;
					Debug.Log("largo");
					determinarAlturaIncorrecto (this.rangoMinimo, this.rangoMaximo);
			}else{
				this.rangoMinimo=this.rangoMinimo+10;
				Debug.Log("corto");
				determinarAlturaIncorrecto (this.rangoMinimo, this.rangoMaximo);
			}

	}
	public void determinarAlturaIncorrecto(int rangoMinimo, int rangoMaximo){
		if(alturaOpcionIncorrecto==0){
			this.rangoMaximo=this.rangoMaximo-5;
			Debug.Log("alto");
		}else{
			this.rangoMinimo=this.rangoMinimo+5;
			Debug.Log("pequeño");
		}
	}
	public void activarPorColorIncorrecto(){
		switch(colorOpcionIncorrecto){
			case 0:						//solo llamar color negro
					elementoActivo = Random.Range(this.rangoMinimo, this.rangoMaximo);
					colorNegro[elementoActivo].SetActive(true);
					break;
			case 1:
					elementoActivo = Random.Range(this.rangoMinimo-19, this.rangoMaximo-19);
					colorVerde[elementoActivo].SetActive(true);
					break;
			case 2:
					elementoActivo = Random.Range(this.rangoMinimo-39, this.rangoMaximo-39);
					colorCafe[elementoActivo].SetActive(true);
					break;
			case 3:
					elementoActivo = Random.Range(this.rangoMinimo-59, this.rangoMaximo-59);
					colorAzul[elementoActivo].SetActive(true);
					break;
			case 4:
					elementoActivo = Random.Range(this.rangoMinimo-79, this.rangoMaximo-79);
					colorRojo[elementoActivo].SetActive(true);
					break;
		}
	}

	public void deteterinarCorrecto(){
		switch(colorOpcion){
			case 0:						//solo llamar color negro
					rangoMinimo=0;
					rangoMaximo=19;
					Debug.Log("color es 1");
					
					determinarAncho(this.rangoMinimo, this.rangoMaximo);
					Debug.Log(Random.Range(rangoMinimo, rangoMaximo));
					break;
			case 1:
					rangoMinimo=20;
					rangoMaximo=39;
					Debug.Log("color es 2");
					color.text = "y verde";
					determinarAncho(this.rangoMinimo, this.rangoMaximo);
					Debug.Log(Random.Range(rangoMinimo, rangoMaximo));
					break;
			case 2:
					rangoMinimo=40;
					rangoMaximo=59;
					Debug.Log("color es 3");
					
					determinarAncho(this.rangoMinimo, this.rangoMaximo);
					Debug.Log(Random.Range(rangoMinimo, rangoMaximo));
					break;
			case 3:
					rangoMinimo=60;
					rangoMaximo=79;
					Debug.Log("color es 4");
					
					determinarAncho(this.rangoMinimo, this.rangoMaximo);
					Debug.Log(Random.Range(rangoMinimo, rangoMaximo));
					break;
			case 4:
					rangoMinimo=80;
					rangoMaximo=99;
					Debug.Log("color es 5");
					
					determinarAncho(this.rangoMinimo, this.rangoMaximo);
					Debug.Log(Random.Range(rangoMinimo, rangoMaximo));
					break;
		}
	}
	public void determinarAncho(int rangoMinimo, int rangoMaximo){
			if(anchoOpcion==0){
					this.rangoMaximo=this.rangoMaximo-10;
					//Debug.Log("largo,");
					
					determinarAltura(this.rangoMinimo, this.rangoMaximo);
			}else{
				this.rangoMinimo=this.rangoMinimo+10;
				//Debug.Log("largo,");
				
				determinarAltura(this.rangoMinimo, this.rangoMaximo);
			}

	}
	public void determinarAltura(int rangoMinimo, int rangoMaximo){
		if(alturaOpcion==0){
			this.rangoMaximo=this.rangoMaximo-5;
			
			//Debug.Log("chico");
		}else{
			this.rangoMinimo=this.rangoMinimo+5;
			//Debug.Log("pequeño");
			
		}
		activarPorColor();
	}
	public void activarPorColor(){
		Debug.Log(this.rangoMinimo + " " + this.rangoMaximo);
		switch(colorOpcion){
			case 0:						//solo llamar color negro
					elementoActivo = Random.Range(this.rangoMinimo, this.rangoMaximo);
					colorNegro[elementoActivo].SetActive(true);
					break;
			case 1:
					elementoActivo = Random.Range(this.rangoMinimo-19, this.rangoMaximo-19);
					colorVerde[elementoActivo].SetActive(true);
					break;
			case 2:
					elementoActivo = Random.Range(this.rangoMinimo-39, this.rangoMaximo-39);
					colorCafe[elementoActivo].SetActive(true);
					break;
			case 3:
					elementoActivo = Random.Range(this.rangoMinimo-59, this.rangoMaximo-59);
					colorAzul[elementoActivo].SetActive(true);
					break;
			case 4:
					elementoActivo = Random.Range(this.rangoMinimo-79, this.rangoMaximo-79);
					colorRojo[elementoActivo].SetActive(true);
					break;
		}
	}
	public void generarEnunciado(){
		switch (colorOpcion){
			case 0:
				color.text = "y negro";
				break;
			case 1:
				color.text = "y verde";
				break;
			case 2:
				color.text = "y cafe";
				break;
			case 3:
				color.text = "y azul";
				break;
			case 4:
				color.text = "y rojo";
				break;
		}
		if(alturaOpcion == 0){
			altura.text = "alto";
			}else{
				altura.text = "pequeño";
		}
		if(anchoOpcion == 0) {
			ancho.text = "ancho";
		}else{
			ancho.text = "angosto";
		}
	}
	public void comprobarCorrecto(int input){
		if (correcto==input){
			ValidacionController.cargarResultadoImage(true);
			RepetGameController.darVuelta();
			StartCoroutine("tiempoEsperaReset");
		}else{
			ValidacionController.cargarResultadoImage(false);
			//StartCoroutine("tiempoEsperaReset");
		}
	}
	public void resetElementos(){
		if(correcto == 1){
			switch(colorOpcion){
			case 0:						//solo llamar color negro
					colorNegro[elementoActivo].SetActive(false);
					break;
			case 1:
					colorVerde[elementoActivo].SetActive(false);
					break;
			case 2:
					colorCafe[elementoActivo].SetActive(false);
					break;
			case 3:
					colorAzul[elementoActivo].SetActive(false);
					break;
			case 4:
					colorRojo[elementoActivo].SetActive(false);
					break;
			}
		} else {
			switch(colorOpcionIncorrecto){
			case 0:						//solo llamar color negro
					colorNegro[elementoActivo].SetActive(false);
					break;
			case 1:
					colorVerde[elementoActivo].SetActive(false);
					break;
			case 2:
					colorCafe[elementoActivo].SetActive(false);
					break;
			case 3:
					colorAzul[elementoActivo].SetActive(false);
					break;
			case 4:
					colorRojo[elementoActivo].SetActive(false);
					break;
			}
		}
		
		ancho.text = "";
		altura.text = "";
		color.text = "";
		iniciarVuelta();
	}
	IEnumerator tiempoEsperaReset(){
		yield return new WaitForSeconds(2);
		resetElementos();
		//activar siguiente para que aparesca botón
	}
	public void destruirInstancia(){
		DestroyGameObject();
	}
	void DestroyGameObject()
    {
        Destroy(gameObject);
    }


}

	

