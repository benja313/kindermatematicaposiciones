﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EncajarAutos : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;

	public GameObject[] elementos = new GameObject[11];
	public GameObject[] elementosOscuros = new GameObject[11];

	private int cantidadElementos;
	private int cantidadElementosOscuros;
	public Text contadorText;
	private int contador;
	private Vector3 posicionAux;
	private int idTrashAux;

	public int cantidadCompleta;
	public int siguienteEscena;
	/*
	private int 
	(534.7, 386.0, 0.0);
	(765.4, 493.9, 0.0);
	(762.2, 386.0, 0.0);
	(996.6, 493.5, 0.0);
	(535.5, 493.5, 0.0);
	(991.8, 390.5, 0.0);
*/

	void Start () {
		//activarElementos();
		contador = 0;
	}
	void Update (){
		contadorText.text = contador.ToString();
	}
	// Update is called once per frame
	public void activarElementos(){
		cantidadElementos = Random.Range(1,11);
		contador = cantidadElementos;
		for(int i=0;i<cantidadElementos;i++){
			elementos[i].SetActive(true);
		}
		for(int i=cantidadElementos;i<11;i++){
			elementosOscuros[i].SetActive(true);
		}
	}
	public void setContador(){
		contador++;
		if(cantidadCompleta==contador){
			StartCoroutine("cargarCorrecto");
		}
	}
	IEnumerator cargarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
				SceneManager.LoadScene(siguienteEscena);
			} else {
				SceneManager.LoadScene("Completada");
			}
	}

	public void setContadorMenos(){
		contador--;
	}
	public void setPositionAux(Vector3 posicion){
		posicionAux = posicion;
	}

	public void setIdTrashAux(int idTrash){
		idTrashAux = idTrash;
		Debug.Log("entra a guardar id Trash");
	}

	public Vector3 getPosicionAux(){
		return posicionAux;
	}
	public void colocarTroncoCorrecto(){
		Debug.Log("entra a activar otro tronco");
		ValidacionController.cargarResultadoImage(true);
		elementos[idTrashAux].SetActive(true);
	}
	public void destroyTrash(){
		Destroy(elementosOscuros[idTrashAux]);
	}
	/*
	public void comprobar(int idItem){
		Debug.Log("Id Item: "+idItem+" idFigura: "+idFigura);
		switch(idItem){
			case 0:
				if(trashCuadrado.getContador()==1){
					Debug.Log("Correcto");
					cuadrado.setPositionTrash(trashCuadrado.getPosition());
				}else{
					Debug.Log("Incorrecto");
					cuadrado.resetDia();
				}
				break;
			case 1:
				if(trashCirculo.getContador()==1){
					Debug.Log("Correcto");
					circulo.setPositionTrash(trashCirculo.getPosition());
				}else{
					Debug.Log("Incorrecto");
					circulo.resetDia();
				}
				break;
			case 2:
				if(trashTriangulo.getContador()==1){
					Debug.Log("Correcto");
					triangulo.setPositionTrash(trashTriangulo.getPosition());
				}else{
					Debug.Log("Incorrecto");
					triangulo.resetDia();
				}
				break;
			case 3:
				if(trashRectangulo.getContador()==1){
					Debug.Log("Correcto");
					rectangulo.setPositionTrash(trashRectangulo.getPosition());
				}else{
					Debug.Log("Incorrecto");
					rectangulo.resetDia();
				}
				break;
		}
		
	}*/

}
