﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Figura3DEncajarUno : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;
	
	public TrashFigura3DEncajarUno trashCubo;
	public TrashFigura3DEncajarUno trashEsfera;
	public TrashFigura3DEncajarUno trashPiramide;

	public ItemsFigura3DEncajarUno cubo;
	public ItemsFigura3DEncajarUno esfera;
	public ItemsFigura3DEncajarUno piramide;
	//public GameObject[] Items = new GameObject[3];
	public GameObject[] figurasGeometricas = new GameObject[3];

	public int siguienteEscena;

	void Start () {
		escogerForma();
	}
	// Update is called once per frame
	public void escogerForma(){
		
		switch(Random.Range(0,3)){
			case 0:
				figurasGeometricas[0].SetActive(true);
				idFigura=0;
				trashCubo.setElemento(0);
				break;
			case 1:
				figurasGeometricas[1].SetActive(true);
				idFigura=1;
				trashEsfera.setElemento(1);
				break;
			case 2:
				figurasGeometricas[2].SetActive(true);
				idFigura=2;
				trashPiramide.setElemento(2);
				break;
		}
	}

	public void comprobar(int idItem){
		Debug.Log("Id Item: "+idItem+" idFigura: "+idFigura);
		switch(idItem){
			case 0:
				if(trashCubo.getContador()==1){
					StartCoroutine("cargarSiguiente");
					cubo.setPositionTrash(trashCubo.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					cubo.resetDia();
				}
				break;
			case 1:
				if(trashEsfera.getContador()==1){
					StartCoroutine("cargarSiguiente");
					esfera.setPositionTrash(trashEsfera.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					esfera.resetDia();
				}
				break;
			case 2:
				if(trashPiramide.getContador()==1){
					StartCoroutine("cargarSiguiente");
					piramide.setPositionTrash(trashPiramide.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					piramide.resetDia();
				}
				break;
		}
	}
	IEnumerator cargarSiguiente(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
			SceneManager.LoadScene(siguienteEscena);
		} else {
			SceneManager.LoadScene("Completada");
		}
	}
	IEnumerator cargarIncorrecto(){
		ValidacionController.cargarResultadoImage(false);
		yield return new WaitForSeconds(2);
	}


}
