﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FiguraEncajar : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;
	public TrashFiguraEncajar trash;
	public ItemsFiguraEncajar cuadrado;
	public ItemsFiguraEncajar circulo;
	public ItemsFiguraEncajar trinagulo;
	public ItemsFiguraEncajar rectangulo;
	public GameObject[] figurasOscuras = new GameObject[4];

	public int siguienteEscena;
	void Start () {
		escogerForma();
	}
	// Update is called once per frame
	public void escogerForma(){
		
		switch(Random.Range(0,4)){
			case 0:
				figurasOscuras[0].SetActive(true);
				idFigura=0;
				trash.setElemento(0);
				break;
			case 1:
				figurasOscuras[1].SetActive(true);
				idFigura=1;
				trash.setElemento(1);
				break;
			case 2:
				figurasOscuras[2].SetActive(true);
				idFigura=2;
				trash.setElemento(2);
				break;
			case 3:
				figurasOscuras[3].SetActive(true);
				idFigura=3;
				trash.setElemento(3);
				break;
		}
	}


	public void comprobar(int id){
		if(trash.getContador()==1&&id==idFigura){
			StartCoroutine("cargarSiguiente");
			switch(id){
				case 0:
					cuadrado.setPositionTrash(trash.getPosition());
					break;
				case 1:
					circulo.setPositionTrash(trash.getPosition());
					break;
				case 2:
					trinagulo.setPositionTrash(trash.getPosition());
					break;
				case 3:
					rectangulo.setPositionTrash(trash.getPosition());
					break;
			}
		}else{
			StartCoroutine("cargarIncorrecto");
			switch(id){
				case 0:
					cuadrado.resetDia();
					break;
				case 1:
					circulo.resetDia();
					break;
				case 2:
					trinagulo.resetDia();
					break;
				case 3:
					rectangulo.resetDia();
					break;
			}
		}
	}
	IEnumerator cargarSiguiente(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
			SceneManager.LoadScene(siguienteEscena);
		} else {
			SceneManager.LoadScene("Completada");
		}
	}
	IEnumerator cargarIncorrecto(){
		ValidacionController.cargarResultadoImage(false);
		yield return new WaitForSeconds(2);
	}

}
