﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComparacionMangerHorizontal : MonoBehaviour {

	// Use this for initialization 
	
	//public GameObject niño;

	public GameObject[] izquierda = new GameObject[26];
	public GameObject[] derecha = new GameObject[26];
	

	private int derechaPosicion;
	private int izquieradaPosicion;


	public ActivarAudio audio;

	//Rango

	private int rangoMinimo;
	private int rangoMaximo;
	public Text direccionText1;
	public Text direccionText2;
	private int posicionElemento;
	public GameObject direccion1;
	public GameObject direccion2;
	public GameObject enunciado1;
	public GameObject enunciado2;

	private int contadorVuelta;

	void Start () {
		contadorVuelta = 0;	
		iniciarElementos();
	}
	public void iniciarElementos(){
		if(contadorVuelta!=7){

			derechaPosicion = Random.Range(0,25);
			izquieradaPosicion = Random.Range(0,25);
			posicionElemento = Random.Range(2,4);
			Debug.Log(posicionElemento);
			ActivarDerecha();
			ActivarIzquierda();
			GenerarEnunciado();
		}else{
			//cargar pantalla de termino;
			SceneManager.LoadScene("Completada");
		}
	}

	public void GenerarEnunciado(){
		switch(this.posicionElemento){
			case 2:
				direccion1.SetActive(true);
				direccionText1.text = "derecha";
				enunciado1.SetActive(true);
				break;
			case 3:
				direccion1.SetActive(true);
				direccionText1.text = "izquierda";
				enunciado1.SetActive(true);
				break;
		}
	}



	public void ActivarDerecha(){
		derecha[this.derechaPosicion].SetActive(true);
	}
	public void ActivarIzquierda(){
		izquierda[this.izquieradaPosicion].SetActive(true);
	}
	
	private void Verificar(int boton){
		audio.play();
		Debug.Log(this.posicionElemento);
		if( this.posicionElemento == boton ){
			contadorVuelta++;
			StartCoroutine("mostrarCorrecto");
		}else{
			ValidacionController.cargarResultadoImage(false);
		}
	}

	public void ocultarElementos(){

		derecha[this.derechaPosicion].SetActive(false);
		izquierda[this.izquieradaPosicion].SetActive(false);
		direccion2.SetActive(false);
		enunciado2.SetActive(false);
		direccion1.SetActive(false);
		enunciado1.SetActive(false);
	}

		public void BotonDerecha(){
		Debug.Log("BotonDerecha");
		Verificar(2);
	}
		public void BotonIzquierda(){
		Debug.Log("BotonIzquierda");
		Verificar(3);
	}

	IEnumerator mostrarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
			ocultarElementos();
			iniciarElementos();
	}

	
}

	

