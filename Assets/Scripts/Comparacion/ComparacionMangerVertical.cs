﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComparacionMangerVertical : MonoBehaviour {

	// Use this for initialization 
	
	//public GameObject niño;

	public GameObject[] Debajo = new GameObject[26];
	public GameObject[] Arriba = new GameObject[26];
	

	private int ArribaPosicion;
	private int DebajoPosicion;


	public ActivarAudio audio;

	//Rango

	private int rangoMinimo;
	private int rangoMaximo;
	public Text direccionText1;
	public Text direccionText2;
	private int posicionElemento;
	public GameObject direccion1;
	public GameObject direccion2;
	public GameObject enunciado1;
	public GameObject enunciado2;

	private int contadorVuelta;

	void Start () {
		contadorVuelta = 0;	
		iniciarElementos();
	}
	public void iniciarElementos(){
		if(contadorVuelta!=7){

			ArribaPosicion = Random.Range(0,25);
			DebajoPosicion = Random.Range(0,25);
			posicionElemento = Random.Range(2,4);
			Debug.Log(posicionElemento);
			ActivarArriba();
			ActivarDebajo();
			GenerarEnunciado();
		}else{
			//cargar pantalla de termino;
			SceneManager.LoadScene("Completada");
		}
	}

	public void GenerarEnunciado(){
		switch(this.posicionElemento){
			case 2:
				direccion1.SetActive(true);
				direccionText1.text = "Arriba";
				enunciado1.SetActive(true);
				break;
			case 3:
				direccion1.SetActive(true);
				direccionText1.text = "Debajo";
				enunciado1.SetActive(true);
				break;
		}
	}



	public void ActivarArriba(){
		Arriba[this.ArribaPosicion].SetActive(true);
	}
	public void ActivarDebajo(){
		Debajo[this.DebajoPosicion].SetActive(true);
	}
	
	private void Verificar(int boton){
		audio.play();
		Debug.Log(this.posicionElemento);
		if( this.posicionElemento == boton ){
			contadorVuelta++;
			StartCoroutine("mostrarCorrecto");
		}else{
			ValidacionController.cargarResultadoImage(false);
		}
	}

	public void ocultarElementos(){

		Arriba[this.ArribaPosicion].SetActive(false);
		Debajo[this.DebajoPosicion].SetActive(false);
		direccion2.SetActive(false);
		enunciado2.SetActive(false);
		direccion1.SetActive(false);
		enunciado1.SetActive(false);
	}

		public void BotonArriba(){
		Debug.Log("BotonArriba");
		Verificar(2);
	}
		public void BotonDebajo(){
		Debug.Log("BotonDebajo");
		Verificar(3);
	}

	IEnumerator mostrarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
			ocultarElementos();
			iniciarElementos();
	}

	
}

	

