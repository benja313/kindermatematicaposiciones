﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComparacionManger : MonoBehaviour {

	// Use this for initialization 
	
	//public GameObject niño;
	
	public GameObject[] detras = new GameObject[10];
	public GameObject[] delante = new GameObject[10];
	public GameObject[] izquierda = new GameObject[26];
	public GameObject[] derecha = new GameObject[26];
	public GameObject[] arriba = new GameObject[26];
	public GameObject[] debajo = new GameObject[26];
	private int detrasPosicion=0;
	private int delantePosicion;
	private int derechaPosicion;
	private int izquieradaPosicion;
	private int arribaPosicion;
	private int debajoPosicion;

	public ActivarAudio audio;

	//Rango

	private int rangoMinimo;
	private int rangoMaximo;
	public Text direccionText1;
	public Text direccionText2;
	private int posicionElemento;
	public GameObject direccion1;
	public GameObject direccion2;
	public GameObject enunciado1;
	public GameObject enunciado2;

	private int contadorVuelta;

	void Start () {
		contadorVuelta = 0;	
		iniciarElementos();
	}
	public void iniciarElementos(){
		if(contadorVuelta!=7){
			detrasPosicion = Random.Range(0,9);
			delantePosicion = Random.Range(0,9);
			derechaPosicion = Random.Range(0,25);
			izquieradaPosicion = Random.Range(0,25);
			arribaPosicion = Random.Range(0,25);
			debajoPosicion = Random.Range(0,25);
			posicionElemento = Random.Range(0,5);
			Debug.Log(posicionElemento);
			ActivarDetras();
			ActivarDelante();
			ActivarDerecha();
			ActivarIzquierda();
			ActivarArriba();
			ActivarDebajo();
			GenerarEnunciado();
		}else{
			//cargar pantalla de termino;
			SceneManager.LoadScene("Completada");
		}
	}

	public void GenerarEnunciado(){
		switch(this.posicionElemento){
			case 0:
				direccion2.SetActive(true);
				direccionText2.text = "detras";
				enunciado2.SetActive(true);
				break;
			case 1:
				direccion2.SetActive(true);
				direccionText2.text = "delante";
				enunciado2.SetActive(true);
				break;
			case 2:
				direccion1.SetActive(true);
				direccionText1.text = "derecha";
				enunciado1.SetActive(true);
				break;
			case 3:
				direccion1.SetActive(true);
				direccionText1.text = "izquierda";
				enunciado1.SetActive(true);
				break;
			case 4:
				direccion2.SetActive(true);
				direccionText2.text = "arriba";
				enunciado2.SetActive(true);
				break;
			case 5:
				direccion2.SetActive(true);
				direccionText2.text = "abajo";
				enunciado2.SetActive(true);
				break;
		}
	}

	private void ActivarDetras(){
		detras[detrasPosicion].SetActive(true);
		Debug.Log("ASCa");
	}
	public void ActivarDelante(){
		delante[delantePosicion].SetActive(true);
	}
	public void ActivarDerecha(){
		derecha[this.derechaPosicion].SetActive(true);
	}
	public void ActivarIzquierda(){
		izquierda[this.izquieradaPosicion].SetActive(true);
	}
	public void ActivarArriba(){
		arriba[this.arribaPosicion].SetActive(true);
	}
	public void ActivarDebajo(){
		debajo[this.debajoPosicion].SetActive(true);
	}
	private void Verificar(int boton){
		audio.play();
		if( this.posicionElemento == boton ){
			contadorVuelta++;
			StartCoroutine("mostrarCorrecto");

		}else{
			ValidacionController.cargarResultadoImage(false);
		}
	}

	public void ocultarElementos(){
		detras[detrasPosicion].SetActive(false);
		delante[delantePosicion].SetActive(false);
		derecha[this.derechaPosicion].SetActive(false);
		izquierda[this.izquieradaPosicion].SetActive(false);
		arriba[this.arribaPosicion].SetActive(false);
		debajo[this.debajoPosicion].SetActive(false);
		direccion2.SetActive(false);
		enunciado2.SetActive(false);
		direccion1.SetActive(false);
		enunciado1.SetActive(false);
	}

	public void BotonDetras(){
		Debug.Log("BotonDetras");
		Verificar(0);
	}
	public void BotonDelante(){
		Debug.Log("BotonDelante");
		Verificar(1);
	}
		public void BotonDerecha(){
		Debug.Log("BotonDerecha");
		Verificar(2);
	}
		public void BotonIzquierda(){
		Debug.Log("BotonIzquierda");
		Verificar(3);
	}
		public void BotonArriba(){
		Debug.Log("BotonArriba");
		Verificar(4);
	}
		public void BotonDebajo(){
		Debug.Log("BotonDebajo");
		Verificar(5);
	}

	IEnumerator mostrarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
			ocultarElementos();
			iniciarElementos();
	}

	
}

	

