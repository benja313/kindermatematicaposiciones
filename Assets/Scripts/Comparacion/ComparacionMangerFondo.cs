﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ComparacionMangerFondo : MonoBehaviour {

	// Use this for initialization 
	
	//public GameObject niño;

	public GameObject[] Detras = new GameObject[10];
	public GameObject[] Adelante = new GameObject[10];
	

	private int AdelantePosicion;
	private int DetrasPosicion;


	public ActivarAudio audio;

	//Rango

	private int rangoMinimo;
	private int rangoMaximo;
	public Text direccionText1;
	public Text direccionText2;
	private int posicionElemento;
	public GameObject direccion1;
	public GameObject direccion2;
	public GameObject enunciado1;
	public GameObject enunciado2;

	private int contadorVuelta;

	void Start () {
		contadorVuelta = 0;	
		iniciarElementos();
	}
	public void iniciarElementos(){
		if(contadorVuelta!=7){

			AdelantePosicion = Random.Range(0,10);
			DetrasPosicion = Random.Range(0,10);
			posicionElemento = Random.Range(2,4);
			Debug.Log(posicionElemento);
			ActivarAdelante();
			ActivarDetras();
			GenerarEnunciado();
		}else{
			//cargar pantalla de termino;
			SceneManager.LoadScene("Completada");
		}
	}

	public void GenerarEnunciado(){
		switch(this.posicionElemento){
			case 2:
				direccion1.SetActive(true);
				direccionText1.text = "Adelante";
				enunciado1.SetActive(true);
				break;
			case 3:
				direccion1.SetActive(true);
				direccionText1.text = "Detras";
				enunciado1.SetActive(true);
				break;
		}
	}



	public void ActivarAdelante(){
		Adelante[this.AdelantePosicion].SetActive(true);
	}
	public void ActivarDetras(){
		Detras[this.DetrasPosicion].SetActive(true);
	}
	
	private void Verificar(int boton){
		audio.play();
		Debug.Log(this.posicionElemento);
		if( this.posicionElemento == boton ){
			contadorVuelta++;
			StartCoroutine("mostrarCorrecto");
		}else{
			ValidacionController.cargarResultadoImage(false);
		}
	}

	public void ocultarElementos(){

		Adelante[this.AdelantePosicion].SetActive(false);
		Detras[this.DetrasPosicion].SetActive(false);
		direccion2.SetActive(false);
		enunciado2.SetActive(false);
		direccion1.SetActive(false);
		enunciado1.SetActive(false);
	}

		public void BotonAdelante(){
		Debug.Log("BotonAdelante");
		Verificar(2);
	}
		public void BotonDetras(){
		Debug.Log("BotonDetras");
		Verificar(3);
	}

	IEnumerator mostrarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
			ocultarElementos();
			iniciarElementos();
	}

	
}

	

