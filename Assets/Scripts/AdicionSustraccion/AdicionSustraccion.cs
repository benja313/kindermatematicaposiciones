﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class AdicionSustraccion : MonoBehaviour {
	private int tipoOperacion;

	private int cantidadGrupo0;
	private int cantidadGrupo1;
	private int cantidadGrupo2;

	public GameObject suma;
	public GameObject resta;
	public GameObject sumaText;
	public GameObject restaText;

	public GeneradorNumeros[] generadorNumeros = new GeneradorNumeros[3];

	public GameObject[] elementos0 = new GameObject [9];
	public GameObject[] elementos1 = new GameObject [9];

	public Resultado resultado;
	public GameObject correcto;
	public GameObject incorrecto;
	public ImageAlpha incorrectoImagen;
	//public GameObject elementos2 = new GameObject [10];
	public int siguienteEscena;

	void Start () {
		tipoOperacion = definirOperacion();
		generaFactores();
		cantidadGrupo2 = cantidadGrupo0 + cantidadGrupo2;
		
		generadorNumeros[0].generarNumeros(cantidadGrupo0);
		generadorNumeros[1].generarNumeros(cantidadGrupo1);
		activarElementos0();
		activarElementos1();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private int generarRandom(int minimo, int max){
		return Random.Range(minimo,max);
	}

	public void activarElementos0(){
		for(int i = 0; i<cantidadGrupo0;i++){
			elementos0[i].SetActive(true);
		}
	}
	public void activarElementos1(){
		for(int i = 0; i<cantidadGrupo1;i++){
			elementos1[i].SetActive(true);
		}
	}
	private int definirOperacion(){
		if(1==Random.Range(0,2)){
			suma.SetActive(true);
			sumaText.SetActive(true);
			return 1;
		}else{
			resta.SetActive(true);
			restaText.SetActive(true);
			return 0;
		}
	}
	public void generaFactores(){
		cantidadGrupo0 = generarRandom(1,10);
		cantidadGrupo1 = generarRandom(1, (10-cantidadGrupo0));
		if(cantidadGrupo0<cantidadGrupo1&&tipoOperacion==0){
			Debug.Log("entra a dar una vuelta");
			generaFactores();
		}
	}
	public void comprobar(){
		switch(tipoOperacion){
			case 0:
				if(resultado.getContador()==cantidadGrupo0-cantidadGrupo1){
					StartCoroutine("cargarSiguiente");
				}else{
					StartCoroutine("cargarIncorrecto");
				}
				break;
			case 1:
				if(resultado.getContador()==cantidadGrupo0+cantidadGrupo1){
					StartCoroutine("cargarSiguiente");
				}else{
					StartCoroutine("cargarIncorrecto");
				}
				break;
		}
	}
	IEnumerator cargarSiguiente(){
		//correcto.SetActive(true);
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
			SceneManager.LoadScene(siguienteEscena);
		} else {
			SceneManager.LoadScene("Completada");
		}
	}
	IEnumerator cargarIncorrecto(){
		ValidacionController.cargarResultadoImage(false);
		yield return new WaitForSeconds(2);

	}
}
