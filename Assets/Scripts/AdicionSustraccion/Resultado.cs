﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resultado : MonoBehaviour {

	// Use this for initialization
	public GameObject[] elementos = new GameObject[10];
	public GameObject[] numeros = new GameObject[12];

	private int contador;


	void Start () {
	contador = 0;
	numeros[0].SetActive(true);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void incrementar(){
		if(contador<10){
			contador++;
			refrescarelementoAumentar(contador);
			refrescarNumeroAumentar(contador);
		}
		Debug.Log(contador);
	}
	public void disminuir(){
		if(contador!=0){
			contador--;
			refrescarelementoDisminuir(contador);
			refrescarNumeroDisminuir(contador);
		}
		Debug.Log(contador);
	}

	public void refrescarelementoAumentar(int numero){
		elementos[numero-1].SetActive(true);
	}
	public void refrescarelementoDisminuir(int numero){
		elementos[numero].SetActive(false);
	}
	public void refrescarNumeroAumentar(int numero){
		if(numero==10){
			numeros[10].SetActive(true);
			numeros[11].SetActive(true);
			numeros[numero-1].SetActive(false);
		}else{
			numeros[numero].SetActive(true);
			numeros[numero-1].SetActive(false);	
		}
		
	}
	public void refrescarNumeroDisminuir(int numero){
		if(numero==9){
			numeros[10].SetActive(false);
			numeros[11].SetActive(false);
			numeros[numero].SetActive(true);
		}
		numeros[numero].SetActive(true);
		numeros[numero+1].SetActive(false);
	}
	public int getContador(){
		return contador;
	}
}
