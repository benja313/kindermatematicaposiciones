﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ManagerSemana : MonoBehaviour {

	// Use this for initialization
	static bool[] elementos = new bool [5];
	public TrashLunes trashLunes;
	public TrashMartes trashMartes;
	public TrashMiercoles trashMiercoles;
	public TrashJueves trashJueves;
	public TrashViernes trashViernes;
	public TrashSabado trashSabado;
	public TrashDomingo trashDomingo;
	public AudioClip clip;
	public AudioClip error;
	public GameObject siguiente;
	public bool correcto;
	private int idDia;

	//Todos los dias
	public ItemsDias diaLunes;
	public ItemsDias diaMartes;
	public ItemsDias diaMiercoles;
	public ItemsDias diaJueves;
	public ItemsDias diaViernes;
	public ItemsDias diaSabado;
	public ItemsDias diaDomingo;

	public int siguienteEscena;

	void Start () {
		//GetComponent<AudioSource> ().playOnAwake = false;
		
		GetComponent<AudioSource>().playOnAwake = false;
		//GetComponent<AudioSource> ().playOnAwake = false;
		//GetComponent<AudioSource> ().clip = error;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void activarGanar(){
		ValidacionController.cargarResultadoImage(true);
		if(trashLunes.getContador()+trashMartes.getContador()+trashMiercoles.getContador()+trashJueves.getContador()+trashViernes.getContador()+trashSabado.getContador()+trashDomingo.getContador()==7){
			//siguiente.SetActive(true);
			StartCoroutine("llamarSiguienteEscena");
		}
		
	}

	IEnumerator llamarSiguienteEscena(){
		yield return new WaitForSeconds(2);
		SceneManager.LoadScene("Completada");
	}

	public bool isEstado(){
		return correcto;
	}
	public void setDia(int id){
		idDia = id;
	}
	public int getDia(){
		return idDia;
	}

	public void resetDia(int dia){
		ValidacionController.cargarResultadoImage(false);
		switch (dia){
			case 1:
				diaLunes.resetDia();
				break;
			case 2:
				diaMartes.resetDia();
				break;
			case 3:
				diaMiercoles.resetDia();
				break;
			case 4:
				diaJueves.resetDia();
				break;
			case 5:
				diaViernes.resetDia();
				break;
			case 6:
				diaSabado.resetDia();
				break;
			case 7:
				diaDomingo.resetDia();
				break;
		}
	}
	public void comprobar(int indice){
		//Manager.elementos[indice];
		//trashAyer.comprobarContenedor();
			switch (indice)
      {
          case 1:
              if(trashLunes.comprobarContenedor()){
								Debug.Log("Buena");
								activarGanar();
								correcto=true;
							}else{
								Debug.Log("mal");
								correcto= false;
								resetDia(getDia());
							
							}
              break;
          case 2:
              if(trashMartes.comprobarContenedor()){
								Debug.Log("Buena");
								activarGanar();
							}else{
								Debug.Log("mal");
								resetDia(getDia());
							
							}
              break;
          case 3:
              if(trashMiercoles.comprobarContenedor()){
								Debug.Log("Buena");
								activarGanar();
							}else{
								resetDia(getDia());
								Debug.Log("mal");
							
							}
              break;
					case 4:
              if(trashJueves.comprobarContenedor()){
								Debug.Log("Buena");
								activarGanar();
							}else{
								resetDia(getDia());
								Debug.Log("mal");
							
							}
              break;
        case 5:
              if(trashViernes.comprobarContenedor()){
								Debug.Log("Buena");
								activarGanar();
							}else{
								resetDia(getDia());
								Debug.Log("mal");
							
							}
              break;
					case 6:
              if(trashSabado.comprobarContenedor()){
								Debug.Log("Buena");
								activarGanar();
							}else{
								resetDia(getDia());
								Debug.Log("mal");
							
							}
              break;
          case 7:
              if(trashDomingo.comprobarContenedor()){
								Debug.Log("Buena");
								activarGanar();
							}else{
								resetDia(getDia());
								Debug.Log("mal");
							
							}
              break;
		
		
}
}
}