﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HistoriaSemana : MonoBehaviour {

	// Use this for initialization
	//public int duracion;
	public AudioClip clip;
	public GameObject siguiente;

	void  Start () {
		GetComponent<AudioSource> ().playOnAwake = false;
		GetComponent<AudioSource> ().clip = clip;
		Debug.Log("Duracion audio"+clip.length);
		//yield return StartCoroutine("activar");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	

	IEnumerator activar()
	{
		// suspend execution for 5 seconds
		yield return new WaitForSeconds(clip.length);
		siguiente.SetActive(true);

	}

}
