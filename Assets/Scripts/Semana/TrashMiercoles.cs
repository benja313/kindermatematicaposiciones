﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TrashMiercoles : MonoBehaviour {
	int contadorAdentro = 0;
	bool estado = false;

	void Start () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		
		if(col.gameObject.tag == "Miercoles"){
				estado = true;
				contadorAdentro++;
		}
	}

	void OnTriggerExit2D (Collider2D col) {
		
		if(col.gameObject.tag == "Miercoles"){
				estado = false;
				contadorAdentro--;
		}
	}	

	public int getContador () {
		return this.contadorAdentro;
	}
	public bool comprobarContenedor(){
		return this.estado;
	}



 

}
