﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class PatronSimple : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;
	public GameObject imagen;
	public TrashPatronSimple trash;
	public ItemsPatronSimple naranja;
	public ItemsPatronSimple sandia;
	public ItemsPatronSimple pera;
	public ItemsPatronSimple frutilla;
	public int siguienteEscena;
	//public GameObject[] figurasOscuras = new GameObject[4];

	void Start () {
		escogerForma();
	}
	// Update is called once per frame
	public void escogerForma(){
		
		switch(0){
			case 0:
				idFigura=0;
				trash.setElemento(0);
				break;
			case 1:
				idFigura=1;
				trash.setElemento(1);
				break;
			case 2:
				idFigura=2;
				trash.setElemento(2);
				break;
			case 3:
				idFigura=3;
				trash.setElemento(3);
				break;
		}
	}


	public void comprobar(int id){
		if(trash.getContador()==1&&id==idFigura){
			StartCoroutine("mostrarCorrecto");
			switch(id){
				case 0:
					naranja.setPositionTrash(trash.getPosition());
					imagen.SetActive(false);
					break;
				case 1:
					sandia.setPositionTrash(trash.getPosition());
					break;
				case 2:
					pera.setPositionTrash(trash.getPosition());
					break;
				case 3:
					frutilla.setPositionTrash(trash.getPosition());
					break;
			}
		}else{
			ValidacionController.cargarResultadoImage(false);
			switch(id){
				case 0:
					naranja.resetDia();
					break;
				case 1:
					sandia.resetDia();
					break;
				case 2:
					pera.resetDia();
					break;
				case 3:
					frutilla.resetDia();
					break;
			}
		}
	}
	IEnumerator mostrarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		SceneManager.LoadScene(siguienteEscena);
	}

}
