﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgualesDistintos : MonoBehaviour {

	// Use this for initialization
	public GameObject[] Animales = new GameObject[36];
	private int izquierada;
	private int derecha;
	public IgualesDistintosManger canvas;

	void Start () {
		izquierada = Random.Range(0,18);
		derecha = Random.Range(19,36);
		ActivarAnimales(izquierada);
		ActivarAnimales(derecha);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void ActivarAnimales(int indice){
		Animales[indice].SetActive(true);
	}
	public void LLamarSiguiente(){
		canvas.activarLongitud();
	}
}
