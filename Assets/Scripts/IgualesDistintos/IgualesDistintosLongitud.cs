﻿using System.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgualesDistintosLongitud : MonoBehaviour {

	// Use this for initialization
	// del 0-7 altos izquierda -- 8-15 chicos izquierda // 16-23 altos derecha // 24-31 chicos derecha.
	public GameObject[] elementos = new GameObject[32];
	private int izquierada;
	private int derecha;
	private bool altoI = false;
	private bool altoD = false;

	public IgualesDistintosManger canvas;


	void Start () {
		activarNuevamente();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void activarNuevamente(){
		elementos[izquierada].SetActive(false);
		elementos[derecha].SetActive(false);
		izquierada = Random.Range(0,15);
		derecha = Random.Range(16,31);
		ActivarElementos(izquierada);
		ActivarElementos(derecha);
	}
	public void ActivarElementos(int indice){
		elementos[indice].SetActive(true);
	}
	public bool comprobarResultado(){
		bool resultado = false;
		if(izquierada>=0&&izquierada<=7){
			altoI=true;
			Debug.Log("izqui es verdad");
		}else{
			altoI = false;
			Debug.Log("izquierada es falso");
		}
if(derecha>=16&&derecha<=23){
			altoD=true;
			Debug.Log("derecha es verdad");
		}else{
			altoD = false;
			Debug.Log("derecha es falso");
		}

		if((altoI&&altoD)||(!altoI&&!altoD)){
			Debug.Log("son iguales");
			resultado = true;
			//correcto.SetActive(true);
		}else{
			resultado = false;
			//incorrecto.SetActive(true);
		}
		return resultado;
	}
	public void coprobarVerdadero (){
		if(comprobarResultado()){
			canvas.activarCorrecto();
			StartCoroutine("llamarSiguiente");
		}else{

			canvas.activarIncorrecto();
		}
	}
	public void comprobarFalso(){
		if(!comprobarResultado()){
			canvas.activarCorrecto();
			StartCoroutine("llamarSiguiente");
		}else{
			canvas.activarIncorrecto();
		}
	}
	
	IEnumerator llamarSiguiente(){
		yield return new WaitForSeconds(2);
		canvas.activarAltura();
	}


}

