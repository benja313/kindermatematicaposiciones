﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IgualesDistintosManger : MonoBehaviour {

	// Use this for initialization
	public GameObject tamaño;
	public GameObject longitud;
	public GameObject altura;
	public GameObject color;
	public GameObject siguiente;
	public GameObject win;
	public GameObject error;
	public ImageAlpha correcto;
	public ImageAlpha incorrecto;
	public int siguienteEscena;
	public int cantidadVuelta;
	private int vueltaActual;

	public IgualesDistintosLongitud longitudScript;
	public IgualesDistintosAltura alturaScript;
	public IgualesDistintosColor colorScript;

	void Start () {
		activarLongitud();		
		vueltaActual = 0;
	}
	private void desactirvarCorrecto(){
		win.SetActive(false);
		error.SetActive(false);
	}
	public void activarTamaño(){
		desactirvarCorrecto();
		tamaño.SetActive(true);
	}
	public void activarLongitud(){
		tamaño.SetActive(false);
		desactirvarCorrecto();
		longitud.SetActive(true);
	}
	public void activarAltura(){
		longitud.SetActive(false);
		desactirvarCorrecto();
		altura.SetActive(true);
	}
	public void activarColor(){
		altura.SetActive(false);
		desactirvarCorrecto();
		color.SetActive(true);
	}
	public void activarVolver(){
		if(cantidadVuelta==vueltaActual){
			SceneManager.LoadScene(siguienteEscena);
		} else {
			vueltaActual++;
			color.SetActive(false);

			longitudScript.activarNuevamente();			
			alturaScript.activarNuevamente();
			colorScript.activarNuevamente();
			activarLongitud();
		}
		
	}
	public void activarCorrecto(){
		StartCoroutine("correctoTime");
	}
	public void activarIncorrecto(){
		StartCoroutine("incorrectoTime");

	}
	IEnumerator correctoTime(){
		win.SetActive(true);
		win.GetComponent<AudioSource>().Play();
		correcto.establecerCeroAlpha();
		yield return new WaitForSeconds(2);
		win.SetActive(false);
	}

	IEnumerator incorrectoTime(){
		error.SetActive(true);
		error.GetComponent<AudioSource>().Play();
		incorrecto.establecerCeroAlpha();
		yield return new WaitForSeconds(2);
		error.SetActive(false);
	}	
}
