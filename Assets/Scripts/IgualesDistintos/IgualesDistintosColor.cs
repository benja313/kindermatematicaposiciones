﻿using System.Collections;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgualesDistintosColor : MonoBehaviour {

	// Use this for initialization
	// del 0-7 altos izquierda -- 8-15 chicos izquierda // 16-23 altos derecha // 24-31 chicos derecha.
	public GameObject[] negro = new GameObject[8];
	public GameObject[] azul = new GameObject[8];
	public GameObject[] verde = new GameObject[8];
	public GameObject[] cafe = new GameObject[8];
	public GameObject[] rojo = new GameObject[8];

	private bool resultado;
	private int colorIzquierda;
	private int colorDerecha;
	public IgualesDistintosManger canvas;
	//private int colorIzquierda;

	private int elementoIzquierda;
	private int elementoDerecha;

	void Start () {
	
		activarNuevamente();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	public void activarNuevamente(){
		desactivarElementos();
		elementoIzquierda = Random.Range(0,3);
		elementoDerecha = Random.Range(4,7);
		ActivarColoresDerecha();
		ActivarColoresIzquierda();
	}
	private void desactivarElementos(){
		switch(colorIzquierda){
			case 0:
				negro[elementoIzquierda].SetActive(false);
				break;
			case 1:
				azul[elementoIzquierda].SetActive(false);
				break;
			case 2:
				cafe[elementoIzquierda].SetActive(false);
				break;
			case 3:
				verde[elementoIzquierda].SetActive(false);
				break;
			case 4:
				rojo[elementoIzquierda].SetActive(false);
				break;
		}
		switch(colorDerecha){
			case 0:
				negro[elementoDerecha].SetActive(false);
				break;
			case 1:
				azul[elementoDerecha].SetActive(false);
				break;
			case 2:
				cafe[elementoDerecha].SetActive(false);
				break;
			case 3:
				verde[elementoDerecha].SetActive(false);
				break;
			case 4:
				rojo[elementoDerecha].SetActive(false);
				break;
		}
	}

	public void ActivarColoresIzquierda(){
		switch(Random.Range(0,4)){
			case 0:
				negro[elementoIzquierda].SetActive(true);
				colorIzquierda = 0;
				break;
			case 1:
				azul[elementoIzquierda].SetActive(true);
				colorIzquierda = 1;
				break;
			case 2:
				cafe[elementoIzquierda].SetActive(true);
				colorIzquierda = 2;
				break;
			case 3:
				verde[elementoIzquierda].SetActive(true);
				colorIzquierda = 3;
				break;
			case 4:
				rojo[elementoIzquierda].SetActive(true);
				colorIzquierda = 4;
				break;
		}
	}

	public void ActivarColoresDerecha(){
		switch(Random.Range(0,5)){
			case 0:
				negro[elementoDerecha].SetActive(true);
				colorDerecha = 0;
				break;
			case 1:
				azul[elementoDerecha].SetActive(true);
				colorDerecha = 1;
				break;
			case 2:
				cafe[elementoDerecha].SetActive(true);
				colorDerecha = 2;
				break;
			case 3:
				verde[elementoDerecha].SetActive(true);
				colorDerecha = 3;
				break;
			case 4:
				rojo[elementoDerecha].SetActive(true);
				colorDerecha = 4;
				break;
		}
	}
	private bool comprobarResultado(){
		if (colorIzquierda==colorDerecha){
			resultado = true;
		} else {
			resultado = false;
		}
		return resultado;
	}
	
	public void coprobarVerdadero (){
		if(comprobarResultado()){
			canvas.activarCorrecto();
			StartCoroutine("llamarSiguiente");
		}else{
			canvas.activarIncorrecto();
		}
	}
	public void comprobarFalso(){
		if(!comprobarResultado()){
			canvas.activarCorrecto();
			StartCoroutine("llamarSiguiente");
		}else{
			canvas.activarIncorrecto();
		}
	}
	IEnumerator llamarSiguiente(){
		yield return new WaitForSeconds(2);
		canvas.activarVolver();
	}



}

