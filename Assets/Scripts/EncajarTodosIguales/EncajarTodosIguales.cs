﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class EncajarTodosIguales : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;
	
	public TrashEncajarTodosIguales trashRectangulo;

	public ItemsEncajarTodosIguales items;

	public GameObject[] elementos = new GameObject[10];
	public GameObject[] elementosOscuros = new GameObject[10];

	private int cantidadElementos;
	private int cantidadElementosOscuros;
	public Text contadorText;
	private int contador;

	private Vector3 posicionAux;
	public int cantidadCompleta;
	public int siguienteEscena;

	void Start () {
		activarElementos();
	}
	void Update (){
		contadorText.text = contador.ToString();
	}
	// Update is called once per frame
	public void activarElementos(){
		cantidadElementos = Random.Range(1,10);
		contador = cantidadElementos;
		for(int i=0;i<cantidadElementos;i++){
			elementos[i].SetActive(true);
		}
		for(int i=cantidadElementos;i<10;i++){
			elementosOscuros[i].SetActive(true);
		}
	}
	public void setContador(){
		contador++;
		if(contador==cantidadCompleta){
			StartCoroutine("cargarCorrecto");
		}
	}
	IEnumerator cargarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
				SceneManager.LoadScene(siguienteEscena);
			} else {
				SceneManager.LoadScene("Completada");
			}
	}
	public void setContadorMenos(){
		contador--;
	}
	public void setPositionAux(Vector3 posicion){
		posicionAux = posicion;
	}

	public Vector3 getPosicionAux(){
		return posicionAux;
	}
	

}
