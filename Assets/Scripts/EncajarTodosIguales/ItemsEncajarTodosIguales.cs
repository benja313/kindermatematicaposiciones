﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class ItemsEncajarTodosIguales : MonoBehaviour {

	public bool dragging = false;
	public bool collision = false;
	private Vector3 position;
	public EncajarTodosIguales manager;
	public int id;
	private Vector3 positionInicial;
	private bool collisionTrash = false;

	public ActivarAudio audio;
	//AudioSource SEAGULLS;
	// Use this for initialization
	void Start () {		
		//Items.playa = false;
		//SEAGULLS = GetComponent<AudioSource>();
		//SEAGULLS.Pause();
		
		positionInicial = transform.position;
	}
	void OnTriggerEnter2D(Collider2D col){
		if(col.gameObject.tag == "trash"){
			collisionTrash = true;
		}else{
			transform.position = positionInicial;
		}
	}
	void OnTriggerExit2D(Collider2D col){
		if(col.gameObject.tag == "trash"){
			collisionTrash = false;
			transform.position = positionInicial;
		}
	}
	
	// Update is called once per frame
	void Update () {	
	}

	public void beginDrag() {
		position = gameObject.transform.position;
		dragging = true;
		audio.play();
	}

	public void drag() {
		transform.position = Input.mousePosition;
		//SEAGULLS.Play(0);
	}

	/*
	private void setEstado(){
		if(Items.playa){
			Items.playa=false;
		}else{
			Items.playa=true;
		}
	}(id);

	void OnCollisionEnter2D(Collision2D  col){
		if(col.gameObject.tag == "Ayer"){
				setEstado();
				Debug.Log(playa);
				Debug.Log("deberia cambiar el estado");
			}
	}*/

	public void drop () {
		if(!collision) {
			gameObject.transform.position = Input.mousePosition;
		}
		if(!collisionTrash){
			resetDia();
		}else{
			transform.position = manager.getPosicionAux();
		}
		//llamar funcion
		//manager.comprobar(id);
		//Debug.Log("estado playa "+ Items.playa);
		dragging = false;
		//Debug.Log(gameObject.transform.position);
	}
	public void resetDia(){
		transform.position = positionInicial;
	}
	public void setPositionTrash(Vector3 trash){
		transform.position = trash;
	}

}
