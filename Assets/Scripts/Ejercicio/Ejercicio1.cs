﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Ejercicio1 : MonoBehaviour {

	private GameObject GM_Instrumentos;
	private ObjetosRandomScript objetosRandomScript;

	public GameObject[] botones  = new GameObject [9];
	private ButtonScript [] buttonScripts = new ButtonScript [9];

 // vidas y gamover 

	public int siguiente ;

	  public GameObject gameOver;
    public GameObject win;
    public GameObject vidaOB;

    public float vida;

    private GameObject GM1;
    private GameObject GM2;
    private GameObject GM3;
	// Use this for initialization
	void Start () {

    this.vida = 3;
    GM1 = GameObject.Find("vida1");
    GM2 = GameObject.Find("vida2");
    GM3 = GameObject.Find("vida3");

		GM_Instrumentos = GameObject.Find("Instrumentos");
		objetosRandomScript = GM_Instrumentos.GetComponent<ObjetosRandomScript>();

		addScritBoton();

		Debug.Log(" " + objetosRandomScript.getNumeroRandom());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private void addScritBoton () {
		for (int i = 0 ; i < 9 ; i++){
			buttonScripts[i] = botones[i].GetComponent<ButtonScript>();
		}
	}


	public void resultado () {
		Debug.Log("random " + objetosRandomScript.getNumeroRandom());
		Debug.Log("colores " + obtenerCantidadPintados());

		if (objetosRandomScript.getNumeroRandom() == obtenerCantidadPintados()) {
			Debug.Log("ganaste");
			StartCoroutine(correcto());
      
		}else {
			Debug.Log("perdiste");
			setVida ();
		}
	}

	private int obtenerCantidadPintados () {

		int contador = 0;
		for (int i = 0 ; i < 9 ; i++){
			if (!buttonScripts[i].getvariableColor()) {
				contador++;
			}
		}

		return contador;
	}


	    public void setVida () {
        this.vida--;
        StartCoroutine(Explode());
        eliminarVidas();
        
    }

    private void eliminarVidas  () {
        if (this.vida == 2) {
            Destroy(GM3);
        } else if (this.vida == 1) {
            Destroy(GM2);
        } else  {
            Destroy(GM1);
            StartCoroutine(finalizar());
            
        }
    }

    IEnumerator Explode()
    {
       this.vidaOB.SetActive(true);
       yield return new WaitForSeconds(0.9f);
       this.vidaOB.SetActive(false);

            
    }

    IEnumerator finalizar()
    {

       yield return new WaitForSeconds(0.9f);
       this.gameOver.SetActive(true);

            
    }

        IEnumerator correcto()
    {
    		this.win.SetActive(true);
       yield return new WaitForSeconds(2.0f);
       SceneManager.LoadScene(siguiente);

            
    }

}
