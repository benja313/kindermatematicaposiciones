﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ObjetosRandomScript : MonoBehaviour {

	public GameObject[] instrumentos = new GameObject [9];

	private int valorMax = 10;
	private int valorMin = 1;

	private int numeroRandom;

	// Use this for initialization
	void Start () {
		numeroRandom = generarRandom();
		Debug.Log(" "+ numeroRandom );
		visualizarInstrumentosRandom (numeroRandom);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private int  generarRandom () {
		return Random.Range (valorMin, valorMax);
	}

	private  void visualizarInstrumentosRandom (int random) {

		for (int i = 0; i < numeroRandom; i++){
			instrumentos[i].SetActive(true);
		}

	}

	public int getNumeroRandom () {
		return this.numeroRandom;
	}
}
