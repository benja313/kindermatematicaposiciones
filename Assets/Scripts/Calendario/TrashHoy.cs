﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TrashHoy : MonoBehaviour {

	// Use this for initialization

	int contadorAdentro = 0;
	bool estado = false;
	private Vector3 posicionInicial;

	void Start () {
		posicionInicial = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnTriggerEnter2D(Collider2D col){
		
		if(col.gameObject.tag == "Hoy"){
				estado = true;
				contadorAdentro++;
		}
	}

	void OnTriggerExit2D (Collider2D col) {
		if(col.gameObject.tag == "Hoy"){
				estado = false;
				contadorAdentro--;
		}
	}
	public void setEstado(){
		estado = false; 
	}

	public int getContador () {
		return this.contadorAdentro;
	}
	public bool comprobarContenedor(){
		return this.estado;
	}




 

}
