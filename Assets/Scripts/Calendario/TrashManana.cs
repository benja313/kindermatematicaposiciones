﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TrashManana : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	int contadorAdentro = 0;
	bool estado = false;

	void OnTriggerEnter2D(Collider2D col){
		Debug.Log("entra ");		if(col.gameObject.tag == "Mañana"){
				estado = true;
				contadorAdentro++;
		}	
	}

	void OnTriggerExit2D (Collider2D col) {
		if(col.gameObject.tag == "Mañana"){
				estado = false;
				contadorAdentro--;
		}
	}	

	public int getContador () {
		return this.contadorAdentro;
	}

	public bool comprobarContenedor(){
		return this.estado;
	}
	public void setEstado(){
		estado = false; 
	}
 

}
