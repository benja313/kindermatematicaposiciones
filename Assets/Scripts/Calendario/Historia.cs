﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Historia : MonoBehaviour {

	// Use this for initialization
	//public int duracion;
	public AudioClip clip;
	public GameObject siguiente0;
	public GameObject siguiente1;
	private bool estadoBoton;
	//public ManagerDias managerDias;

	void  Start () {
		GetComponent<AudioSource> ().playOnAwake = false;
		GetComponent<AudioSource> ().clip = clip;
		Debug.Log("Duracion audio"+clip.length);
		estadoBoton = true;
		StartCoroutine("activar");
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	
	public void cargarSiguiente(int siguiente){
		if(siguiente==1){
			SceneManager.LoadScene(16);
		}else if(siguiente==2){
			SceneManager.LoadScene(16);
		}
		
	}

	IEnumerator activar()
	{
		// suspend execution for 5 seconds
		yield return new WaitForSeconds(clip.length);
		if(ManagerDias.aux==1){
			siguiente1.SetActive(true);
		}else if(ManagerDias.aux==0) {
			siguiente0.SetActive(true);
		}

	}

}
