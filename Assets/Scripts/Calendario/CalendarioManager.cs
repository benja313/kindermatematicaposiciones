﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CalendarioManager : MonoBehaviour {

	// Use this for initialization
	static bool[] elementos = new bool [5];
	public TrashAyer trashAyer;
	public TrashHoy trashHoy;
	public TrashManana trashManana;
	public AudioClip clip;
	public AudioClip error;
	public GameObject siguiente;
	public Items[] items = new Items[5];

	void Start () {
		//GetComponent<AudioSource> ().playOnAwake = false;
		
		GetComponent<AudioSource>().playOnAwake = false;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void activarGanar(){
		Debug.Log("entro a activarGanar");
		Debug.Log(trashAyer.getContador());
		Debug.Log(trashHoy.getContador());
		Debug.Log(trashManana.getContador());
		if(trashAyer.getContador()==2&&trashHoy.getContador()==2&&trashManana.getContador()==1){
			StartCoroutine("llamarSiguienteEscena");
		}
	}

	IEnumerator llamarSiguienteEscena(){
		yield return new WaitForSeconds(2);
		SceneManager.LoadScene("Completada");
	}
	
	public void comprobar(int indice){
		//Manager.elementos[indice];
		//trashAyer.comprobarContenedor();
		if(indice==3||indice==4){
			if(trashAyer.comprobarContenedor()){
			Debug.Log("Buena");
			activarGanar();
			trashHoy.setEstado();
			ValidacionController.cargarResultadoImage(true);
			
		}else{
			items[indice-3].resetPosicion();
			
			ValidacionController.cargarResultadoImage(false);
			
		}
		}
		if(indice==1||indice==2){
			if(trashHoy.comprobarContenedor()){
			Debug.Log("Buena");
			activarGanar();
			trashHoy.setEstado();
			ValidacionController.cargarResultadoImage(true);
			
		}else{
			items[indice+1].resetPosicion();
			
			ValidacionController.cargarResultadoImage(false);
			
		}
		}
		if(indice==6){
			if(trashManana.comprobarContenedor()){
			ValidacionController.cargarResultadoImage(true);
			activarGanar();
			trashManana.setEstado();
			
		}else{
			items[indice-2].resetPosicion();
			ValidacionController.cargarResultadoImage(false);
		}
		}
		
}
}