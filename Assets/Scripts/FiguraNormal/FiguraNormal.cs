﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class FiguraNormal : MonoBehaviour {

	// Use this for initialization
	public Text figuraText;
	private string figura;
	private int idFigura;
	public TrashFiguraNormal trash;
	public ItemsFiguraNormal cuadrado;
	public ItemsFiguraNormal circulo;
	public ItemsFiguraNormal triagulo;
	public ItemsFiguraNormal rectangulo;


	public int siguienteEscena;

	void Start () {
		
		figuraText.text = escogerForma() ;
	}
	
	// Update is called once per frame
	public string escogerForma(){
		string forma = "";
		switch(Random.Range(0,4)){
			case 0:
				forma = "cuadrado";
				idFigura=0;
				trash.setElemento(0);
				break;
			case 1:
				forma = "circulo";
				idFigura=1;
				trash.setElemento(1);
				break;
			case 2:
				forma = "triagulo";
				idFigura=2;
				trash.setElemento(2);
				break;
			case 3:
				forma = "rectangulo";
				idFigura=3;
				trash.setElemento(3);
				break;
		}
		return forma;
	}

	public void comprobar(int id){
		if(trash.getContador()==1&&id==idFigura){
			StartCoroutine("cargarSiguiente");
		}else{
			StartCoroutine("cargarIncorrecto");
			Debug.Log("son distintos");
			switch(id){
				case 0: 
					cuadrado.resetDia();
					break;
				case 1: 
					circulo.resetDia();
					break;
				case 2: 
					triagulo.resetDia();
					break;
				case 3: 
					rectangulo.resetDia();
					break;
			}
		}
	}
	IEnumerator cargarSiguiente(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
			SceneManager.LoadScene(siguienteEscena);
		} else {
			SceneManager.LoadScene("Completada");
		}
	}
	IEnumerator cargarIncorrecto(){
		ValidacionController.cargarResultadoImage(false);
		yield return new WaitForSeconds(2);
	}
}
