﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
public class FiguraEncajarTodos : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;
	
	public TrashFiguraEncajarTodos trashCuadrado;
	public TrashFiguraEncajarTodos trashCirculo;
	public TrashFiguraEncajarTodos trashTriangulo;
	public TrashFiguraEncajarTodos trashRectangulo;

	public ItemsFiguraEncajarTodos cuadrado;
	public ItemsFiguraEncajarTodos circulo;
	public ItemsFiguraEncajarTodos triangulo;
	public ItemsFiguraEncajarTodos rectangulo;

	public int siguienteEscena;

	void Start () {
		escogerForma();
	}
	// Update is called once per frame
	public void escogerForma(){
		
		switch(Random.Range(0,4)){
			case 0:
				idFigura=0;
				trashCuadrado.setElemento(0);
				break;
			case 1:
				idFigura=1;
				trashCirculo.setElemento(1);
				break;
			case 2:
				idFigura=2;
				trashTriangulo.setElemento(2);
				break;
			case 3:
				idFigura=3;
				trashRectangulo.setElemento(3);
				break;
		}
	}

	public void comprobar(int idItem){
		Debug.Log("Id Item: "+idItem+" idFigura: "+idFigura);
		switch(idItem){
			case 0:
				if(trashCuadrado.getContador()==1){
					StartCoroutine("cargarSiguiente");
				cuadrado.setPositionTrash(trashCuadrado.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					cuadrado.resetDia();
				}
				break;
			case 1:
				if(trashCirculo.getContador()==1){
					StartCoroutine("cargarSiguiente");
					circulo.setPositionTrash(trashCirculo.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					circulo.resetDia();
				}
				break;
			case 2:
				if(trashTriangulo.getContador()==1){
					StartCoroutine("cargarSiguiente");
					triangulo.setPositionTrash(trashTriangulo.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					triangulo.resetDia();
				}
				break;
			case 3:
				if(trashRectangulo.getContador()==1){
					StartCoroutine("cargarSiguiente");
					rectangulo.setPositionTrash(trashRectangulo.getPosition());
				}else{
					StartCoroutine("cargarIncorrecto");
					rectangulo.resetDia();
				}
				break;
		}
	}
	IEnumerator cargarSiguiente(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(trashRectangulo.getContador()==trashTriangulo.getContador()&&trashCirculo.getContador()==trashCuadrado.getContador()&&trashCuadrado.getContador()==1&&trashTriangulo.getContador()==1){
			if(siguienteEscena != 0){
				SceneManager.LoadScene(siguienteEscena);
			} else {
				SceneManager.LoadScene("Completada");
			}
		}
		
	}
	IEnumerator cargarIncorrecto(){
		ValidacionController.cargarResultadoImage(false);
		yield return new WaitForSeconds(2);
	}

}
