﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Figura3D : MonoBehaviour {

	// Use this for initialization
	public Text figuraText;
	private string figura;
	private int idFigura;
	public TrashFigura3D trash;
	public ItemsFigura3D cubo;
	public ItemsFigura3D esfera;
	public ItemsFigura3D piramide;

	public int siguienteEscena;

	void Start () {
		
		figuraText.text = escogerForma() ;
	}
	
	// Update is called once per frame
	public string escogerForma(){
		string forma = "";
		switch(Random.Range(0,3)){
			case 0:
				forma = "cubo";
				idFigura=0;
				trash.setElemento(0);
				break;
			case 1:
				forma = "esfera";
				idFigura=1;
				trash.setElemento(1);
				break;
			case 2:
				forma = "piramide";
				idFigura=2;
				trash.setElemento(2);
				break;
		}
		return forma;
	}

	public void comprobar(int id){
		if(trash.getContador()==1&&id==idFigura){
			StartCoroutine("cargarSiguiente");
		}else{
			StartCoroutine("cargarIncorrecto");
			switch(id){
				case 0:
					cubo.resetDia();
					break;
				case 1:
					esfera.resetDia();
					break;
				case 2:
					piramide.resetDia();
					break;
			}
		}
	}
	IEnumerator cargarSiguiente(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
			SceneManager.LoadScene(siguienteEscena);
		} else {
			SceneManager.LoadScene("Completada");
		}
	}
	IEnumerator cargarIncorrecto(){
		ValidacionController.cargarResultadoImage(false);
		yield return new WaitForSeconds(2);
	}

}
