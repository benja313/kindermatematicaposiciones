﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecuenciaNumerosAbejas	 : MonoBehaviour {

	// Use this for initialization
	private int numeroFaltante;
	private int numeroInicial;
	public GameObject[] elementos = new GameObject[11];
	public Vector3[] posiciones = new Vector3[4];

	private int opcion0;
	private int opcion1;
	private int opcion2;
	private int opcion3;

	public Text opcion0Text;
	public Text opcion1Text;
	public Text opcion2Text;
	public Text opcion3Text;

	private int correcto;


	void Start () {

		numeroInicial = Random.Range(0,7);
		numeroFaltante = obtenerIndiceFaltante();
		/*
		for(int i = 0; i<4;i++){
			Debug.Log(elementos[i].transform.position);
		}*/
		posiciones[0] = new Vector3(412.0f, 262.0f, 0.0f);
		posiciones[1] = new Vector3(522.0f, 279.0f, 0.0f);
		posiciones[2] = new Vector3(623.0f, 252.0f, 0.0f);
		posiciones[3] = new Vector3(727.0f, 284.0f, 0.0f);		
		activarElementos();
		correcto = opcionCorrecta();

		opcion0Text.text = opcion0.ToString();
		opcion1Text.text = opcion1.ToString();
		opcion2Text.text = opcion2.ToString();
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public int obtenerIndiceFaltante(){
		return Random.Range(numeroInicial, numeroInicial+3);;
	}
	public void activarElementos(){
		int cont = 0;

		for(int i = numeroInicial; i<(numeroInicial+4);i++){
			if(i!=numeroFaltante){
				elementos[i].SetActive(true);
				setPositionElement(cont, i);
			}else{
				elementos[10].SetActive(true);
				setPositionElement(cont, 10);
			}			
			cont++;
		}
	}
	public void setPositionElement(int indicePosicion, int indiceElementos){
		elementos[indiceElementos].transform.position = posiciones[indicePosicion];
	}

	public int opcionCorrecta(){
		int aux = Random.Range(0,4) ;
		switch(aux){
			case 0:
				aux = 0;
				this.opcion0 = this.numeroFaltante;
				this.opcion1 = generarOpciones();
				this.opcion2 = generarOpciones();
				this.opcion3 = generarOpciones();
				break;
			case 1:
				aux = 1;
				this.opcion1 = this.numeroFaltante;
				this.opcion0 = generarOpciones();
				this.opcion2 = generarOpciones();
				this.opcion3 = generarOpciones();
				break;
			case 2:
				aux = 2;
				this.opcion2 = this.numeroFaltante;
				this.opcion0 = generarOpciones();
				this.opcion1 = generarOpciones();
				this.opcion3 = generarOpciones();
				break;
			case 3:
				aux = 3;
				this.opcion3 = this.numeroFaltante;
				this.opcion0 = generarOpciones();
				this.opcion1 = generarOpciones();
				this.opcion2 = generarOpciones();
				break;
		}
		return aux;
	}

	public int generarOpciones(){
		int aux = Random.Range(0,10);
		if(aux==this.opcion0 || aux==this.opcion1 || aux==this.opcion2 || aux==this.opcion3){
			aux = generarOpciones();
			Debug.Log("entra incorrecto");
		}else{		
			//generarOpciones();
		}
		return aux;
	}
	public void comprobar(int id){
		if(this.correcto == id){
			Debug.Log("correcto");
		}else{
			Debug.Log("incorrecto");
		}
	}

}
