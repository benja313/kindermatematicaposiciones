﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using	UnityEngine.UI;

public class IdentificarNumero : MonoBehaviour {

	// Use this for initialization
	public GameObject[] elementos = new GameObject[20];
	private int cantidadElementos;
	private int opcion0;
	private int opcion1;
	private int opcion2;
	private int opcion3;

	public Text opcion0Text;
	public Text opcion1Text;
	public Text opcion2Text;
	public Text opcion3Text;

	private int correcto;

	void Start () {
		cantidadElementos = generarRandom();
		correcto = opcionCorrecta();
		activarElementos();
		opcion0Text.text = opcion0.ToString();
		opcion1Text.text = opcion1.ToString();
		opcion2Text.text = opcion2.ToString();
		opcion3Text.text = opcion3.ToString();
		Debug.Log("cantidadElementos"+cantidadElementos);


	}
	// Update is called once per frame
	void Update () {
		
	}
	public void activarElementos(){
		for(int i=0; i<cantidadElementos;i++){
			elementos[i].SetActive(true);
		}
	}
	private int opcionCorrecta(){
		int aux = Random.Range(0,4) ;
		switch(aux){
			case 0:
				aux = 0;
				this.opcion0 = this.cantidadElementos;
				this.opcion1 = generarOpciones();
				this.opcion2 = generarOpciones();
				this.opcion3 = generarOpciones();
				break;
			case 1:
				aux = 1;
				this.opcion1 = this.cantidadElementos;
				this.opcion0 = generarOpciones();
				this.opcion2 = generarOpciones();
				this.opcion3 = generarOpciones();
				break;
			case 2:
				aux = 2;
				this.opcion2 = this.cantidadElementos;
				this.opcion0 = generarOpciones();
				this.opcion1 = generarOpciones();
				this.opcion3 = generarOpciones();
				break;
			case 3:
				aux = 3;
				this.opcion3 = this.cantidadElementos;
				this.opcion0 = generarOpciones();
				this.opcion1 = generarOpciones();
				this.opcion2 = generarOpciones();
				break;
		}
		return aux;
	}
	public void comprobar(int id){
		if(this.correcto == id){
			Debug.Log("correcto");
		}else{
			Debug.Log("incorrecto");
		}
	}
	public int generarOpciones(){
		int aux = generarRandom();
		if(aux==this.opcion0 || aux==this.opcion1 || aux==this.opcion2 || aux==this.opcion3){
			aux = generarOpciones();
			Debug.Log("entra incorrecto");
		}else{		
			//generarOpciones();
		}
		return aux;
	}
	public int generarRandom(){
		return Random.Range(1,15);
	}
}
