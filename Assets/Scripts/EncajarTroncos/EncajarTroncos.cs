﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

public class EncajarTroncos : MonoBehaviour {

	// Use this for initialization
	private string figura;
	private int idFigura;
	

	

	public GameObject[] elementos = new GameObject[11];
	public GameObject[] elementosOscuros = new GameObject[11];

	private int cantidadElementos;
	private int cantidadElementosOscuros;
	//public Text ;
	public TextMeshProUGUI contadorText;
	private int contador;
	private Vector3 posicionAux;
	private int idTrashAux;

	public int cantidadCompleta;
	public int siguienteEscena;

	void Start () {
		activarElementos();
	}
	void Update (){
		contadorText.text = contador.ToString();
	}
	// Update is called once per frame
	public void activarElementos(){
		cantidadElementos = Random.Range(1,11);
		contador = cantidadElementos;
		for(int i=0;i<cantidadElementos;i++){
			elementos[i].SetActive(true);
		}
		for(int i=cantidadElementos;i<11;i++){
			elementosOscuros[i].SetActive(true);
		}
	}
	public void setContador(){
		contador++;
		if(contador== cantidadCompleta){
			StartCoroutine("cargarCorrecto");
		}
	}
	IEnumerator cargarCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
				SceneManager.LoadScene(siguienteEscena);
			} else {
				SceneManager.LoadScene("Completada");
			}
	}
	public void setContadorMenos(){
		contador--;
	}
	public void setPositionAux(Vector3 posicion){
		posicionAux = posicion;
	}

	public void setIdTrashAux(int idTrash){
		idTrashAux = idTrash;
		Debug.Log("entra a guardar id Trash");
	}

	public Vector3 getPosicionAux(){
		return posicionAux;
	}
	public void colocarTroncoCorrecto(){
		ValidacionController.cargarResultadoImage(true);
		elementos[idTrashAux].SetActive(true);
	}
	public void destroyTrash(){
		Destroy(elementosOscuros[idTrashAux]);
	}

}
