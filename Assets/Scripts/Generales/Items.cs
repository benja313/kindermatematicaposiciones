﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class Items : MonoBehaviour {

	public bool dragging = false;
	public bool collision = false;
	private Vector3 position;
	public CalendarioManager manager;
	public ActivarAudio activarAudio;
	public int id;

	private Vector3 posicionInicial;
	//AudioSource SEAGULLS;
	// Use this for initialization
	void Start () {		
		//Items.playa = false;
		//SEAGULLS = GetComponent<AudioSource>();
		//SEAGULLS.Pause();
		posicionInicial = transform.position;

	}
	
	// Update is called once per frame
	void Update () {		
	}

	public void beginDrag() {
		position = gameObject.transform.position;
		dragging = true;
		activarAudio.play();
	}

	public void drag() {
		transform.position = Input.mousePosition;

		//SEAGULLS.Play(0);
	}


	public void drop () {
		if(!collision) {
			gameObject.transform.position = Input.mousePosition;
		}
		//llamar funcion
		manager.comprobar(id);
		//Debug.Log("estado playa "+ Items.playa);
		dragging = false;
		//Debug.Log(gameObject.transform.position);
	}
	public void resetPosicion(){
		transform.position = posicionInicial;
	}


}
