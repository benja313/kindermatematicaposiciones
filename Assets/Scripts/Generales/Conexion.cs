﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Networking;

public class Conexion : MonoBehaviour {

	// Use this for initialization
	 void Start()
    {
        StartCoroutine(Upload());
    }

    IEnumerator Upload()
    {
        WWWForm form = new WWWForm();
        form.AddField("actividad_id", "1");
        form.AddField("numero", "2");
        form.AddField("vidas", "1");
        form.AddField("estudiante_id", "1");
        form.AddField("bloque_id", "1");


        using (UnityWebRequest www = UnityWebRequest.Post("http://acamykidsapi.acamy.cl/api/ejercicios", form))
        {
            yield return www.SendWebRequest();

            if (www.isNetworkError || www.isHttpError)
            {
                Debug.Log(www.error);
            }
            else
            {
                Debug.Log("subido");
            }
        }
}}
