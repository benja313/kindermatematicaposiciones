﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class ResultadoFinal : MonoBehaviour {

	public Text timer_t;
	public Text coins_t;

	int tiempofinal = 1200;

	// Use this for initialization
	void Start () {
		coins_t.text = ProjectVars.Instance.coin.ToString();

		timer_t.text = tiempoOcupado();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	private string tiempoOcupado() {
		int resultado = tiempofinal - ((int)ProjectVars.Instance.time);
		return resultado.ToString();
	}
}
