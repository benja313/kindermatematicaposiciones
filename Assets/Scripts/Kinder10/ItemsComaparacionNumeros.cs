﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//[RequireComponent(typeof(AudioSource))]
public class ItemsComaparacionNumeros : MonoBehaviour {

	public bool dragging = false;
	public bool collision = false;
	private Vector3 position;
	private Vector3 rotacion;
	public ComparacionNumeros manager;
	public int id;
	private Vector3 posicionInicial;
	public ActivarAudio ActivarAudio;

	//AudioSource SEAGULLS;
	// Use this for initialization
	void Start () {		
		//Items.playa = false;
		//SEAGULLS = GetComponent<AudioSource>();
		//SEAGULLS.Pause();
		posicionInicial = transform.position;

	}
	
	// Update is called once per frame
	void Update () {		
	}

	public void beginDrag() {
		//position = gameObject.transform.position;

		dragging = true;
		ActivarAudio.play();
	}

	public void drag() {
		//Debug.Log(Input.mousePosition);
		transform.position = Input.mousePosition;
	}


	public void drop () {
		if(!collision) {
			gameObject.transform.position = Input.mousePosition;
		}
		//llamar funcion
		manager.comprobar(id);
		//Debug.Log("estado playa "+ Items.playa);
		dragging = false;
		//Debug.Log(gameObject.transform.position);
	}
	public void resetPosition(){
		transform.position = posicionInicial;
	}


}
