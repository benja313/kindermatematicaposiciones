﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ComparacionCantidadesMenos : MonoBehaviour {

	public GameObject[] elementos = new GameObject[8];
	public GameObject[] elementos1 = new GameObject[8];
	public GameObject[] elementos2 = new GameObject[8];

	private int cantidadElementos;
	private int cantidadElementos1;
	private int cantidadElementos2;

	private int vueltaActual;
	public int cantidadRepetir;
	public int siguienteEscena;
	void Start () {
		vueltaActual = 0;
		iniciarElementos();
	}
	
	// Update is called once per frame
	void Update () {
	}

	private void iniciarElementos(){
		desactivarElementos();
		generarCantidades();
		activarElementos();
	}
	private void generarCantidades(){
		cantidadElementos = generarRandom();
		cantidadElementos1 = generarRandom();
		cantidadElementos2 = generarRandom();
		if(cantidadElementos==cantidadElementos1||cantidadElementos==cantidadElementos2){
			generarCantidades();
		}else if(cantidadElementos2 == cantidadElementos1){
			generarCantidades();
		}
	}
	private void activarElementos(){
		for(int i = 0; i<cantidadElementos;i++){
			elementos[i].SetActive(true);
		}
		for(int i = 0; i<cantidadElementos1;i++){
			elementos1[i].SetActive(true);
		}
		for(int i = 0; i<cantidadElementos2;i++){
			elementos2[i].SetActive(true);
		}
	}
	private void desactivarElementos(){
		for(int i = 0; i<cantidadElementos;i++){
			elementos[i].SetActive(false);
		}
		for(int i = 0; i<cantidadElementos1;i++){
			elementos1[i].SetActive(false);
		}
		for(int i = 0; i<cantidadElementos2;i++){
			elementos2[i].SetActive(false);
		}
	}
	private int generarRandom(){
		return Random.Range(1,9);
	}
	public void comprobar(float angulo){
		if(angulo>0 &&angulo<120f && cantidadElementos1<cantidadElementos2 && cantidadElementos1<cantidadElementos){
			Debug.Log("Gano 2 tercio");
			cargarNuevamente();
		}else if ((( angulo>-180 && angulo<-120 )||  angulo>120f) && cantidadElementos<cantidadElementos2 && cantidadElementos1>cantidadElementos){
			Debug.Log("Gano 1 tercio");
			cargarNuevamente();
		}else if (angulo<0f && angulo>-120 && cantidadElementos>cantidadElementos2 && cantidadElementos1>cantidadElementos2){
			Debug.Log("Gano 3 tercio");
			cargarNuevamente();
		}else{
			ValidacionController.cargarResultadoImage(false);
		}
	}
	private void cargarNuevamente(){
		ValidacionController.cargarResultadoImage(true);
		if(vueltaActual == cantidadRepetir){
				StartCoroutine("llamarSiguienteEscena");
			} else{
				StartCoroutine("refrescarElementos");
			}
		vueltaActual++;
	}
	IEnumerator refrescarElementos(){
		yield return new WaitForSeconds(2);
		iniciarElementos();
	}
	IEnumerator llamarSiguienteEscena(){
		yield return new WaitForSeconds(2);
		SceneManager.LoadScene(siguienteEscena);
	}
}
