﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ComparacionCantidades : MonoBehaviour {

	public GameObject[] elementos = new GameObject[8];
	public GameObject[] elementos1 = new GameObject[8];
	public GameObject[] elementos2 = new GameObject[8];

	private int cantidadElementos;
	private int cantidadElementos1;
	private int cantidadElementos2;
	void Start () {
		generarCantidades();
		activarElementos();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void generarCantidades(){
		cantidadElementos = generarRandom();
		cantidadElementos1 = generarRandom();
		cantidadElementos2 = generarRandom();
		if(cantidadElementos==cantidadElementos1||cantidadElementos==cantidadElementos2){
			generarCantidades();
		}else if(cantidadElementos2 == cantidadElementos1){
			generarCantidades();
		}
	}
	private void activarElementos(){
		for(int i = 0; i<cantidadElementos;i++){
			elementos[i].SetActive(true);
		}
		for(int i = 0; i<cantidadElementos1;i++){
			elementos1[i].SetActive(true);
		}
		for(int i = 0; i<cantidadElementos2;i++){
			elementos2[i].SetActive(true);
		}
	}
	private int generarRandom(){
		return Random.Range(1,9);
	}
	public void comprobar(float angulo){
		if(angulo>0 &&angulo<120f && cantidadElementos1>cantidadElementos2 && cantidadElementos1>cantidadElementos){
			Debug.Log("Gano 2 tercio");
		}else if ((( angulo>-180 && angulo<-120 )||  angulo>120f) && cantidadElementos>cantidadElementos2 && cantidadElementos1<cantidadElementos){
			Debug.Log("Gano 1 tercio");
		}else if (angulo<0f && angulo>-120 && cantidadElementos<cantidadElementos2 && cantidadElementos1<cantidadElementos2){
			Debug.Log("Gano 3 tercio");
		}else{
			Debug.Log("perdio");
		}
	}
}
