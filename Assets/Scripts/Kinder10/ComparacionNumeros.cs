﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ComparacionNumeros : MonoBehaviour {

	public GameObject[] elementos = new GameObject[10];
	public GameObject[] elementos1 = new GameObject[10];
	public TrashComparacionNumeros trash;
	public ItemsComaparacionNumeros[] items = new ItemsComaparacionNumeros[3];

	private int cantidadElementos;
	private int cantidadElementos1;
	private int signo;

	public int siguienteEscena;
	
	void Start () {
		generarCantidades();
		activarElementos();
		signo = determinarSigno();
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	private void generarCantidades(){
		cantidadElementos = generarRandom();
		cantidadElementos1 = generarRandom();
	}
	private void activarElementos(){
		for(int i = 0; i<cantidadElementos;i++){
			elementos[i].SetActive(true);
		}
		for(int i = 0; i<cantidadElementos1;i++){
			elementos1[i].SetActive(true);
		}
	}
	private int generarRandom(){
		return Random.Range(1,11);
	}
	private int determinarSigno(){
		if(cantidadElementos<cantidadElementos1){
			return 0;
		}else if(cantidadElementos>cantidadElementos1){
			return 1;
		}else{
			return 2;
		}
	}
	public void comprobar(int idItem){
		if (idItem==signo && trash.getContador()==1){
			StartCoroutine("llamarSiguienteEscena");
		}else{
			ValidacionController.cargarResultadoImage(false);
			items[idItem].resetPosition();
		}
	}

	IEnumerator llamarSiguienteEscena(){
		ValidacionController.cargarResultadoImage(true);
		yield return new WaitForSeconds(2);
		if(siguienteEscena != 0){
			SceneManager.LoadScene(siguienteEscena);
			} else {
				SceneManager.LoadScene("Completada");
			}
		
	}
	
}
